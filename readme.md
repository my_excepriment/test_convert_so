# Тестирование конвертации пакета в so файл для использования в качестве защиты.
https://www.programmersought.com/article/65324291519/

1. Ставим либы:
    ```
    sudo apt-get  install  build-essential
    pip install cython
    ```
2. Берем файл 
    ```
    python py-setup.py package
    ```
3. Получаем папку build
4. Копируем туда main.py
5. Создаем питоновские файлы file_one.py file_two.py обеспечивающие импорт из so файлов.
6. Запускаем mian.py по пути ./src/build/src/main.py
