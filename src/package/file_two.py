import logging



def print_list_array(count: int) -> None:
    logger = logging.getLogger().getChild(__name__)
    for i in range(count):
        logger.info('Count item is %s', i)