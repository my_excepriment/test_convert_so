import logging

from src.package.file_two import print_list_array


def print_log_info(text: str) -> None:
    logger = logging.getLogger().getChild(__name__)

    logger.info(msg=text)


def print_array_from_two_file() -> None:
    print_list_array(3)