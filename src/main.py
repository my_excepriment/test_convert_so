import logging
import logging.config
from src.package.file_one import print_array_from_two_file, print_log_info

def config_log() -> None:
    LOG_LEVEL = 'DEBUG'
    LOGGING_CONFIG = {
        'version': 1,
        'disable_existing_loggers': True,
        'formatters': {
            'standard': {
                'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
            },
        },
        'handlers': {
            'default': {
                'level': LOG_LEVEL,
                'formatter': 'standard',
                'class': 'logging.StreamHandler',
                'stream': 'ext://sys.stdout',
            },
        },
        'loggers': {
            '': {  # root logger
                'handlers': ['default'],
                'level': LOG_LEVEL,
                'propagate': True,
            }
        },
    }
    logging.config.dictConfig(LOGGING_CONFIG)

if __name__=='__main__':
    config_log()
    print_log_info(text='test log message')
    print_array_from_two_file()
